package main

import (
	"fmt"
	"github.com/awalterschulze/gographviz"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"strings"
)

const defaultImage = "alpine/terragrunt:latest"

func main() {
	var template string
	var directory string
	var input string
	var output string

	cmd := &cobra.Command{}

	cmd.Flags().StringVarP(&template, "template", "", "", "")
	cmd.Flags().StringVarP(&directory, "dir", "", ".", "")
	cmd.Flags().StringVarP(&input, "input", "", "terragrunt.dot", "")
	cmd.Flags().StringVarP(&output, "out", "", ".terragrunt.gitlab-ci.yml", "")

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		return execute(directory, template, input, output)
	}

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func execute(directory, template, input, output string) error {
	pipeline := map[string]any{}

	if template != "" {
		content, err := os.ReadFile(template)
		if err != nil {
			return err
		}

		if err := yaml.Unmarshal(content, pipeline); err != nil {
			return err
		}
	}

	content, err := os.ReadFile(input)
	if err != nil {
		return err
	}

	graphAst, err := gographviz.Parse(content)
	if err != nil {
		return err
	}

	graph := gographviz.NewGraph()
	if err := gographviz.Analyse(graphAst, graph); err != nil {
		return err
	}

	for _, n := range graph.Nodes.Nodes {
		name := strings.Trim(n.Name, `"`)
		var needs []string
		if e, ok := graph.Edges.SrcToDsts[n.Name]; ok {
			for i, _ := range e {
				needs = append(needs, strings.Trim(i, `"`))
			}
		}

		job := Job{
			Stage: "deploy",
			Image: Image{
				Name:       getImage(),
				Entrypoint: []string{""},
			},
			Variables: map[string]string{
				"TERRAGRUNT_NON_INTERACTIVE": "true",
			},
			Script: []string{
				fmt.Sprintf("cd %s", filepath.Join(directory, name)),
				fmt.Sprintf("terragrunt validate"),
				fmt.Sprintf("terragrunt plan -out=tf.plan -compact-warnings"),
				fmt.Sprintf("terragrunt apply tf.plan"),
			},
			Needs: needs,
		}

		pipeline[name] = job
	}

	data, err := yaml.Marshal(pipeline)
	if err != nil {
		return err
	}

	if err := os.WriteFile(output, data, 0664); err != nil {
		return err
	}

	return nil
}

func getImage() string {
	image := os.Getenv("TERRAGRUNT_IMAGE")
	if image != "" {
		return image
	}
	return defaultImage
}

type Job struct {
	Stage     string            `yaml:"stage,omitempty"`
	Image     Image             `yaml:"image,omitempty"`
	Script    []string          `yaml:"script,omitempty"`
	Needs     []string          `yaml:"needs,omitempty"`
	Variables map[string]string `yaml:"variables,omitempty"`
}

type Image struct {
	Name       string   `yaml:"name"`
	Entrypoint []string `yaml:"entrypoint,omitempty"`
}
